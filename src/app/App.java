package app;

import engine.DisplayManager;
import engine.MasterRenderer;
import engine.loaders.Loader;
import engine.loaders.OBJLoader;
import entities.Entity;
import entities.Player;
import entities.ambient.*;
import models.TexturedModel;
import models.textures.ModelTexture;
import models.textures.TerrainTexture;
import models.textures.TerrainTexturePack;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class App {

    public static void main(String[] args) {
        DisplayManager.createDisplay("RG");

        final Loader loader = Loader.of();

        final List<Light> lights = createLights();

        final Terrain terrain = createTerrain(loader);

        final List<Terrain> terrains = new ArrayList<>();

        terrains.add(terrain);

        final List<Entity> entities = createEntities(loader, terrain);

        final WaterFrameBuffer frameBuffer = WaterFrameBuffer.of();

        final MasterRenderer renderer = MasterRenderer.of(frameBuffer);

        final Player player = createPlayer(loader);

        entities.add(player);

        final Camera camera = Camera.Builder.of(player, new Vector3f(0, 0 ,0)).build();

        final Sky sky = Sky.of(loader);

//        final Water water = Water.of(loader, 210, -430, 14);

        final Water water = Water.of(loader, 0, 0, -8);

        renderer.getSkyboxRenderer().process(sky);

        renderer.getWaterRenderer().process(water);

        while(!Display.isCloseRequested()){
            player.move(terrain);
            camera.move();

            frameBuffer.bindReflectionFrameBuffer();
            float distance = 2 * (camera.getPosition().getY() - water.getHeight());
            camera.getPosition().y -= distance;
            camera.invertPitch();
            renderer.renderScene(entities, terrains, lights, camera, new Vector4f(0, 1, 0, -water.getHeight() + 1f));
            camera.getPosition().y += distance;
            camera.invertPitch();

            frameBuffer.bindRefractionFrameBuffer();
            renderer.renderScene(entities, terrains, lights, camera, new Vector4f(0, -1, 0, water.getHeight()));

            GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
            frameBuffer.unbindCurrentFrameBuffer();
            renderer.renderScene(entities, terrains, lights, camera, new Vector4f(0, -1, 0, 1500));
            renderer.renderWater(lights, camera);

            DisplayManager.updateDisplay();
        }

        frameBuffer.cleanUp();
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();
    }

    private static List<Light> createLights() {
        final List<Light> lights = new ArrayList<>();

        lights.add(Light.of(new Vector3f(0, 10000, -7000), new Vector3f(0.8f, 0.8f, 0.8f)));
        lights.add(Light.of(new Vector3f(185, 10, -293), new Vector3f(2, 2, 2), new Vector3f(1, 0.01f, 0.002f)));
        lights.add(Light.of(new Vector3f(370, 17, -300), new Vector3f(2, 2, 2), new Vector3f(1, 0.01f, 0.002f)));
        lights.add(Light.of(new Vector3f(293, 7, -305), new Vector3f(2, 2, 2), new Vector3f(1, 0.01f, 0.002f)));

        return lights;
    }

    private static Terrain createTerrain(final Loader loader) {
        final TerrainTexture backgroundTexture = TerrainTexture.of(loader.loadTexture("grassy.png"));
        final TerrainTexture rTexture = TerrainTexture.of(loader.loadTexture("dirt.png"));
        final TerrainTexture gTexture = TerrainTexture.of(loader.loadTexture("grassy.png"));
        final TerrainTexture bTexture = TerrainTexture.of(loader.loadTexture("pinkFlowers.png"));

        final TerrainTexturePack texturePack = TerrainTexturePack.of(backgroundTexture, rTexture, gTexture, bTexture);
        final TerrainTexture blendMap = TerrainTexture.of(loader.loadTexture("map/blendMap.png"));
        final File heightMap = new File("resources/textures/map/heightMap.png");
        final TerrainMaps terrainMaps = TerrainMaps.of(blendMap, heightMap);

        return Terrain.Builder.of(loader, texturePack, terrainMaps).gridX(-0.5f).gridZ(-0.8f).build();
    }

    private static List<Entity> createEntities(final Loader loader, final Terrain terrain) {
        final ModelTexture treeModelTexture = ModelTexture.Builder.of(loader.loadTexture("tree.png")).build();

        final TexturedModel treeModel = TexturedModel.of(OBJLoader.loadOBJModel("tree.obj", loader), treeModelTexture);

        final ModelTexture fernModelTexture = ModelTexture.Builder.of(loader.loadTexture("fern.png"))
                                                                  .transparent(true).fakeLight(true).numberOfRows(2).build();
        final TexturedModel fern = TexturedModel.of(OBJLoader.loadOBJModel("fern.obj", loader), fernModelTexture);

        final List<Entity> entities = new ArrayList<>();

        final Random random = new Random();

        for(int i = 0; i < 1000; i++) {
            if (i % 4 == 0) {
                final float x = random.nextFloat() * 800 - 400;

                final float z = random.nextFloat() * -600;

                if (terrain.getHeightOfTerrain(x, z) < -8)
                    continue;

                final float y = terrain.getHeightOfTerrain(x, z);

                entities.add(Entity.Builder.of(treeModel, new Vector3f(x, y, z)).scale(10).build());
            }

            if (i % 3 == 0) {
                final float x = random.nextFloat() * 800 - 400;

                final float z = random.nextFloat() * -600;

                if (terrain.getHeightOfTerrain(x, z) < -8)
                    continue;

                final float y = terrain.getHeightOfTerrain(x, z);

                entities.add(Entity.Builder.of(fern, new Vector3f(x, y, z)).scale(0.6f).textureIndex(random.nextInt(4)).build());
            }
        }

        return entities;
    }

    private static Player createPlayer(final Loader loader) {
        final ModelTexture stanfordBunnyTexture = ModelTexture.Builder.of(loader.loadTexture("playerTexture.png")).build();

        final TexturedModel stanfordBunny = TexturedModel.of(OBJLoader.loadOBJModel("stanfordBunny.obj", loader), stanfordBunnyTexture);

        return Player.PlayerBuilder.of(stanfordBunny, new Vector3f(100, 0, -50)).scale(0.3f).build();
    }

}

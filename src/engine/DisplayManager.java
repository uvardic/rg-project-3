package engine;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.*;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class DisplayManager {

    private static final int DISPLAY_WIDTH = 1280;

    private static final int DISPLAY_HEIGHT = 780;

    private static final int FPS_CAP = 60;

    private static long lastFrameTime;

    private static float delta;

    private DisplayManager() {}

    public static void createDisplay(final String title) {
        checkNotNull(title);

        checkArgument(!title.isEmpty(), "title can't be an empty String");

        final ContextAttribs attribs = new ContextAttribs(3, 2).withForwardCompatible(true)
                                                                                       .withProfileCore(true);

        try {
            Display.setDisplayMode(new DisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT));
            Display.create(new PixelFormat().withSamples(8), attribs);
            Display.setTitle(title);
            GL11.glEnable(GL13.GL_MULTISAMPLE);
        } catch (LWJGLException e) { e.printStackTrace(); }

        GL11.glViewport(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);
        lastFrameTime = getCurrentTime();
    }

    public static void updateDisplay() {
        Display.sync(FPS_CAP);
        Display.update();

        final long currentFrameTime = getCurrentTime();

        delta = (currentFrameTime - lastFrameTime) / 1000f;
        lastFrameTime = currentFrameTime;
    }

    public static float getFrameTimeSeconds() {
        return delta;
    }

    private static long getCurrentTime() {
        return Sys.getTime() * 1000 / Sys.getTimerResolution();
    }

    public static void closeDisplay() {
        Display.destroy();
    }

}

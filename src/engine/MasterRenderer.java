package engine;

import engine.renders.EntityRenderer;
import engine.renders.SkyboxRenderer;
import engine.renders.TerrainRenderer;
import engine.renders.WaterRenderer;
import entities.Entity;
import entities.ambient.Camera;
import entities.ambient.Light;
import entities.ambient.Terrain;
import entities.ambient.WaterFrameBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;
import util.Maths;

import java.util.List;

import static util.Preconditions.checkNotNull;

public class MasterRenderer {

    public static final float RED = 0.5444f;

    public static final float GREEN = 0.62f;

    public static final float BLUE = 0.69f;

    private final EntityRenderer entityRenderer;

    private final TerrainRenderer terrainRenderer;

    private final SkyboxRenderer skyboxRenderer;

    private final WaterRenderer waterRenderer;

    private MasterRenderer(final WaterFrameBuffer frameBuffer) {
        enableCulling();

        final Matrix4f projectionMatrix = Maths.createProjectionMatrix();

        this.entityRenderer  = EntityRenderer.of(projectionMatrix);
        this.terrainRenderer = TerrainRenderer.of(projectionMatrix);
        this.skyboxRenderer  = SkyboxRenderer.of(projectionMatrix);
        this.waterRenderer   = WaterRenderer.of(projectionMatrix, frameBuffer);
    }

    public static MasterRenderer of(final WaterFrameBuffer frameBuffer) {
        return new MasterRenderer(frameBuffer);
    }

    public static void enableCulling() {
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }

    public static void disableCulling() {
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    public void renderScene(final List<Entity> entities, final List<Terrain> terrains, final List<Light> lights,
                            final Camera camera, final Vector4f clipPlane) {

        entities.forEach(entityRenderer::process);
        terrains.forEach(terrainRenderer::process);

        render(lights, camera, clipPlane);
    }

    private void render(final List<Light> lights, final Camera camera, final Vector4f clipPlane) {
        checkNotNull(lights);
        checkNotNull(camera);

        prepare();

        entityRenderer.render(lights, camera, clipPlane);
        terrainRenderer.render(lights, camera, clipPlane);
        skyboxRenderer.render(lights, camera, clipPlane);
    }

    public void renderWater(final List<Light> lights, final Camera camera) {
        checkNotNull(lights);
        checkNotNull(camera);

        // TODO temp
        waterRenderer.render(lights, camera, new Vector4f(0, 0, 0, 0));
    }

    private void prepare() {
        GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(RED, GREEN, BLUE, 1);
    }

    public void cleanUp() {
        entityRenderer.cleanUpShader();
        terrainRenderer.cleanUpShader();
    }

    public EntityRenderer getEntityRenderer() {
        return entityRenderer;
    }

    public TerrainRenderer getTerrainRenderer() {
        return terrainRenderer;
    }

    public SkyboxRenderer getSkyboxRenderer() {
        return skyboxRenderer;
    }

    public WaterRenderer getWaterRenderer() {
        return waterRenderer;
    }
}

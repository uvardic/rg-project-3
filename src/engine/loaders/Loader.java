package engine.loaders;

import models.RawModel;
import models.textures.TextureData;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import org.newdawn.slick.opengl.PNGDecoder;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static util.Preconditions.checkNotNull;

public class Loader {

    private final List<Integer> vaos = new ArrayList<>();

    private final List<Integer> vbos = new ArrayList<>();

    private final List<Integer> textures = new ArrayList<>();

    private Loader() {}

    public static Loader of() {
        return new Loader();
    }

    public RawModel loadToVAO(final float[] positions, final float[] textureCoords, final float[] normals, final int[] indices) {
        checkNotNull(positions);
        checkNotNull(textureCoords);
        checkNotNull(indices);

        final int vaoID = createVAO();

        bindIndicesBuffer(indices);
        storeDataInAttributeList(0, 3, positions);
        storeDataInAttributeList(1, 2, textureCoords);
        storeDataInAttributeList(2, 3, normals);
        unbindVAO();

        return RawModel.of(vaoID, indices.length);
    }

    private int createVAO() {
        final int vaoID = GL30.glGenVertexArrays();

        vaos.add(vaoID);

        GL30.glBindVertexArray(vaoID);

        return vaoID;
    }

    private void bindIndicesBuffer(final int[] indices) {
        final int vboID = GL15.glGenBuffers();

        final IntBuffer buffer = storeDataInIntBuffer(indices);

        vbos.add(vboID);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
    }

    private IntBuffer storeDataInIntBuffer(final int[] data) {
        final IntBuffer buffer = BufferUtils.createIntBuffer(data.length);

        buffer.put(data);
        buffer.flip();

        return buffer;
    }

    private void storeDataInAttributeList(final int attributeNumber, final int coordinateSize, final float[] data) {
        final int vboID = GL15.glGenBuffers();

        final FloatBuffer buffer = storeDataInFloatBuffer(data);

        vbos.add(vboID);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0 , 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }

    private FloatBuffer storeDataInFloatBuffer(final float[] data) {
        final FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);

        buffer.put(data);
        buffer.flip();

        return buffer;
    }

    private void unbindVAO() {
        GL30.glBindVertexArray(0);
    }

    public RawModel loadToVAO(final float[] positions, final int dimensions) {
        final int vaoID = createVAO();

        storeDataInAttributeList(0, dimensions, positions);
        unbindVAO();

        return RawModel.of(vaoID, positions.length / dimensions);
    }

    public int loadTexture(final String fileName) {
        try {
            final String filePath = "resources/textures/" + fileName;

            final Texture texture = TextureLoader.getTexture("PNG", new FileInputStream(filePath));

            textures.add(texture.getTextureID());

            enableMipMapping();

            if (GLContext.getCapabilities().GL_EXT_texture_filter_anisotropic) {
                final float amount = Math.min(4f, GL11.glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT));

                GL11.glTexParameterf(GL11.GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
            }

            return texture.getTextureID();

        } catch (IOException ex) { ex.printStackTrace(); }

        throw new RuntimeException();
    }

    private void enableMipMapping() {
        final float mipMappingFactor = 0;

        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
        GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, mipMappingFactor);
    }

    public int loadCubeMap(final List<File> textureFiles) {
        final int textureID = generateTextureID();

        textureFiles.forEach(textureFile -> loadCubeMapFace(textureFile, textureFiles.indexOf(textureFile)));

        GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);

        textures.add(textureID);

        return textureID;
    }

    private int generateTextureID() {
        final int textureID = GL11.glGenTextures();

        GL13.glActiveTexture(textureID);
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, textureID);

        return textureID;
    }

    private void loadCubeMapFace(final File textureFile, final int indexOfTextureFile) {
        final TextureData data = decodeTextureFile(textureFile);

        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + indexOfTextureFile,
                0, GL11.GL_RGBA, data.getWidth(), data.getHeight(),
                0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, data.getBuffer());
    }

    private TextureData decodeTextureFile(final File textureFile) {
        try (final FileInputStream inputStream = new FileInputStream(textureFile)){

            final PNGDecoder decoder = new PNGDecoder(inputStream);

            final int width = decoder.getWidth(), height = decoder.getHeight();

            final ByteBuffer buffer = ByteBuffer.allocateDirect(4 * width * height);

            decoder.decode(buffer, width * 4, PNGDecoder.RGBA);
            buffer.flip();

            return TextureData.of(width, height, buffer);

        } catch (IOException e) { e.printStackTrace(); }

        throw new RuntimeException("Error while decoding texture file");
    }

    public void cleanUp() {
        vaos.forEach(GL30::glDeleteVertexArrays);

        vbos.forEach(GL15::glDeleteBuffers);

        textures.forEach(GL11::glDeleteTextures);
    }

}

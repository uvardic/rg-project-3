package engine.loaders;

import models.RawModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class OBJLoader {

    private static List<Vector3f> vertices;

    private static List<Vector2f> textures;

    private static List<Vector3f> normals;

    private static List<Integer> indices;

    private static float[] verticesArray;

    private static float[] normalsArray;

    private static float[] textureArray;

    private static int[] indicesArray;

    private OBJLoader() {}

    public static RawModel loadOBJModel(final String fileName, final Loader loader) {
        resetListsAndArrays();

        final String filePath = "resources/objects/" + fileName;

        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)))) {

            parseOBJFile(reader);

        } catch (IOException ex) { ex.printStackTrace(); }

        verticesArray = new float[vertices.size() * 3];
        indicesArray  = new int[indices.size()];

        switchDataToArrays();

        return loader.loadToVAO(verticesArray, textureArray, normalsArray, indicesArray);
    }

    private static void resetListsAndArrays() {
        vertices = new ArrayList<>();
        textures = new ArrayList<>();
        normals  = new ArrayList<>();
        indices  = new ArrayList<>();

        verticesArray = null;
        normalsArray  = null;
        textureArray  = null;
        indicesArray  = null;
    }

    private static void parseOBJFile(final BufferedReader reader) throws IOException {
        while (true) {
            final String line = reader.readLine();

            final String[] currentLine = line.split(" ");

            if (line.startsWith("v "))
                vertices.add(createVector(currentLine[1], currentLine[2], currentLine[3]));

            else if (line.startsWith("vt"))
                textures.add(createVector(currentLine[1], currentLine[2]));

            else if (line.startsWith("vn "))
                normals.add(createVector(currentLine[1], currentLine[2], currentLine[3]));

            else if (line.startsWith("f ")) {
                textureArray = new float[vertices.size() * 2];
                normalsArray = new float[vertices.size() * 3];
                parseFaces(reader, line);
                break;
            }
        }
    }

    private static Vector3f createVector(final String x, final String y, final String z) {
        return new Vector3f(parseFloat(x), parseFloat(y), parseFloat(z));
    }

    private static Vector2f createVector(final String x, final String y) {
        return new Vector2f(parseFloat(x), parseFloat(y));
    }

    private static void parseFaces(final BufferedReader reader, String line) throws IOException {
        while (line != null) {
            if (!line.startsWith("f ")) {
                line = reader.readLine();
                continue;
            }

            final String[] currentLine = line.split(" ");

            final String[] vertex1 = currentLine[1].split("/");
            final String[] vertex2 = currentLine[2].split("/");
            final String[] vertex3 = currentLine[3].split("/");

            processVertex(vertex1);
            processVertex(vertex2);
            processVertex(vertex3);

            line = reader.readLine();
        }
    }

    private static void processVertex(final String[] vertexData) {
        final int currentVertexPointer = parseInt(vertexData[0]) - 1;

        final Vector2f currentTexture = textures.get(parseInt(vertexData[1]) - 1);

        final Vector3f currentNormal = normals.get(parseInt(vertexData[2]) - 1);

        indices.add(currentVertexPointer);

        textureArray[currentVertexPointer * 2]     = currentTexture.getX();
        textureArray[currentVertexPointer * 2 + 1] = 1 - currentTexture.getY();

        normalsArray[currentVertexPointer * 3]     = currentNormal.getX();
        normalsArray[currentVertexPointer * 3 + 1] = currentNormal.getZ();
        normalsArray[currentVertexPointer * 3 + 2] = currentNormal.getY();
    }

    private static void switchDataToArrays() {
        int vertexPointer = 0;

        for (Vector3f vertex : vertices) {
            verticesArray[vertexPointer++] = vertex.getX();
            verticesArray[vertexPointer++] = vertex.getY();
            verticesArray[vertexPointer++] = vertex.getZ();
        }

        IntStream.range(0, indices.size()).forEach(i -> indicesArray[i] = indices.get(i));
    }

}

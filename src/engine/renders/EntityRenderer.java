package engine.renders;

import engine.MasterRenderer;
import entities.Entity;
import entities.ambient.Camera;
import entities.ambient.Light;
import models.RawModel;
import models.TexturedModel;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;
import shaders.entity.EntityShader;
import models.textures.ModelTexture;
import util.Maths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.Preconditions.checkNotNull;

public class EntityRenderer implements Renderer<Entity> {

    private final EntityShader shader = EntityShader.of();

    private final Map<TexturedModel, List<Entity>> entities = new HashMap<>();

    private EntityRenderer(final Matrix4f projectionMatrix) {
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
    }

    public static EntityRenderer of(final Matrix4f projectionMatrix) {
        checkNotNull(projectionMatrix);

        return new EntityRenderer(projectionMatrix);
    }

    @Override
    public void render(final List<Light> lights, final Camera camera, final Vector4f clipPlane) {
        shader.start();
        shader.loadClipPlane(clipPlane);
        shader.loadSkyColour(MasterRenderer.RED, MasterRenderer.GREEN, MasterRenderer.BLUE);
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);

        entities.keySet().forEach(this::renderTexturedModel);

        shader.stop();
        entities.clear();
    }

    private void renderTexturedModel(final TexturedModel texturedModel) {
        prepareTexturedModel(texturedModel);

        entities.get(texturedModel).forEach(entity -> renderEntity(entity, texturedModel));

        unbindTexturedModel();
    }

    private void prepareTexturedModel(final TexturedModel texturedModel) {
        final RawModel model = texturedModel.getRawModel();

        final ModelTexture texture = texturedModel.getTexture();

        GL30.glBindVertexArray(model.getVaoID());

        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        if (texture.isTransparent())
            MasterRenderer.disableCulling();

        shader.loadNumberOfRows(texture.getNumberOfRows());
        shader.loadFakeLight(texture.isFakeLight());
        shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());

        GL13.glActiveTexture(GL13.GL_TEXTURE0);

        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getTexture().getTextureID());
    }

    private void renderEntity(final Entity entity, final TexturedModel texturedModel) {
        prepareInstance(entity);

        GL11.glDrawElements(GL11.GL_TRIANGLES, texturedModel.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
    }

    private void prepareInstance(final Entity entity) {
        final Matrix4f transformationMatrix = Maths.createTransformationMatrix(entity.getPosition(), entity.getRotations(),
                entity.getScale());

        shader.loadTransformationMatrix(transformationMatrix);
        shader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset());
    }

    private void unbindTexturedModel() {
        MasterRenderer.enableCulling();

        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);

        GL30.glBindVertexArray(0);
    }

    @Override
    public void process(final Entity subject) {
        checkNotNull(subject);

        final List<Entity> batch = entities.get(subject.getTexturedModel());

        if (batch != null)
            batch.add(subject);

        else {
            final List<Entity> newBatch = new ArrayList<>();

            newBatch.add(subject);
            entities.put(subject.getTexturedModel(), newBatch);
        }
    }

    @Override
    public void cleanUpShader() {
        shader.cleanUp();
    }

    public EntityShader getShader() {
        return shader;
    }
}

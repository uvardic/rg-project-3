package engine.renders;

import entities.ambient.Camera;
import entities.ambient.Light;
import org.lwjgl.util.vector.Vector4f;

import java.util.List;

interface Renderer<T> {

     void render(final List<Light> lights, final Camera camera, final Vector4f clipPlane);

     void process(final T subject);

     void cleanUpShader();

}

package engine.renders;

import engine.DisplayManager;
import engine.MasterRenderer;
import entities.ambient.Camera;
import entities.ambient.Light;
import entities.ambient.Sky;
import models.RawModel;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import shaders.skybox.SkyboxShader;

import java.util.ArrayList;
import java.util.List;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class SkyboxRenderer implements Renderer<Sky> {

    private final SkyboxShader shader = SkyboxShader.of();

    private final List<Sky> skies = new ArrayList<>();

    private SkyboxRenderer(final Matrix4f projectionMatrix) {
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.connectTextureUnits();
        shader.stop();
    }

    public static SkyboxRenderer of(final Matrix4f projectionMatrix) {
        checkNotNull(projectionMatrix);

        return new SkyboxRenderer(projectionMatrix);
    }

    @Override
    public void render(final List<Light> lights, final Camera camera, final Vector4f clipPlane) {
        shader.start();
        shader.loadViewMatrix(camera);
        shader.loadFogColour(new Vector3f(MasterRenderer.RED, MasterRenderer.GREEN, MasterRenderer.BLUE));

        skies.forEach(sky -> bindTextures(lights, sky));

        unbindTextures();
        shader.stop();
    }

    private float time;

    private int dayTexture;

    private int nightTexture;

    private float blendFactor;

    private void bindTextures(final List<Light> lights, final Sky sky) {
        final RawModel skyModel = sky.getRawModel();

        GL30.glBindVertexArray(skyModel.getVaoID());
        GL20.glEnableVertexAttribArray(0);

        cycleTextures(lights, sky);

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, dayTexture);

        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, nightTexture);

        shader.loadBlendFactor(blendFactor);

        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, skyModel.getVertexCount());
    }

    private void cycleTextures(final List<Light> lights, final Sky sky) {
        time += DisplayManager.getFrameTimeSeconds() * 1000;
        time %= 24000;

//        lights.remove(0);

        if (time >= 0 && time < 5000) {
            dayTexture = sky.getNightTextureID();
            nightTexture = sky.getNightTextureID();
            blendFactor = time / 5000;

//            lights.add(0, Light.of(new Vector3f(0, 10000, -7000), new Vector3f(0.1f, 0.1f, 0.1f)));
        }

        else if (time >= 5000 && time < 8000) {
            dayTexture = sky.getNightTextureID();
            nightTexture = sky.getDayTextureID();
            blendFactor = (time - 5000) / (8000 - 5000);

//            lights.add(0, Light.of(new Vector3f(0, 10000, -7000), new Vector3f(0.4f, 0.4f, 0.4f)));
        }

        else if (time >= 8000 && time < 21000) {
            dayTexture = sky.getDayTextureID();
            nightTexture = sky.getDayTextureID();
            blendFactor = (time - 8000) / (21000 - 8000);

//            lights.add(0, Light.of(new Vector3f(0, 10000, -7000), new Vector3f(0.8f, 0.8f, 0.8f)));
        }

        else {
            dayTexture = sky.getDayTextureID();
            nightTexture = sky.getNightTextureID();
            blendFactor = (time - 21000) / (24000 - 21000);

//            lights.add(0, Light.of(new Vector3f(0, 10000, -7000), new Vector3f(0.3f, 0.3f, 0.3f)));
        }
    }

    private void unbindTextures() {
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
    }

    @Override
    public void process(Sky subject) {
        checkNotNull(subject);
        checkArgument(!skies.contains(subject), "sky already in a list of skies");

        skies.add(subject);
    }

    @Override
    public void cleanUpShader() {
        shader.cleanUp();
    }
}

package engine.renders;

import engine.MasterRenderer;
import entities.EntityRotations;
import entities.ambient.Camera;
import entities.ambient.Light;
import entities.ambient.Terrain;
import models.RawModel;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import shaders.terrain.TerrainShader;
import models.textures.TerrainTexturePack;
import util.Maths;

import java.util.ArrayList;
import java.util.List;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class TerrainRenderer implements Renderer<Terrain> {

    private final List<Terrain> terrains = new ArrayList<>();

    private final TerrainShader shader = TerrainShader.of();

    private TerrainRenderer(final Matrix4f projectionMatrix) {
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.connectTextureUnits();
        shader.stop();
    }

    public static TerrainRenderer of(final Matrix4f projectionMatrix) {
        checkNotNull(projectionMatrix);

        return new TerrainRenderer(projectionMatrix);
    }

    @Override
    public void render(final List<Light> lights, final Camera camera, final Vector4f clipPlane) {
        shader.start();
        shader.loadClipPlane(clipPlane);
        shader.loadSkyColour(MasterRenderer.RED, MasterRenderer.GREEN, MasterRenderer.BLUE);
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);

        terrains.forEach(this::renderTerrain);

        shader.stop();
        terrains.clear();
    }

    private void renderTerrain(final Terrain terrain) {
        prepareTerrain(terrain);
        loadModelMatrix(terrain);
        GL11.glDrawElements(GL11.GL_TRIANGLES, terrain.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
        unbindTexturedModel();
    }

    private void prepareTerrain(final Terrain terrain) {
        final RawModel model = terrain.getRawModel();

        GL30.glBindVertexArray(model.getVaoID());

        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        bindTextures(terrain);

        // TODO temp
        shader.loadShineVariables(1, 0);
    }

    private void bindTextures(final Terrain terrain) {
        final TerrainTexturePack texturePack = terrain.getTexturePack();

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getBackgroundTexture().getTextureID());

        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getRTexture().getTextureID());

        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getGTexture().getTextureID());

        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getBTexture().getTextureID());

        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, terrain.getBlendMap().getTextureID());

    }

    private void loadModelMatrix(final Terrain terrain) {
        final Matrix4f transformationMatrix = Maths.createTransformationMatrix(new Vector3f(terrain.getGridX(), 0, terrain.getGridZ()),
                EntityRotations.Builder.of().build(), 1);

        shader.loadTransformationMatrix(transformationMatrix);
    }

    private void unbindTexturedModel() {
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);

        GL30.glBindVertexArray(0);
    }

    @Override
    public void process(final Terrain subject) {
        checkNotNull(subject);
        checkArgument(!terrains.contains(subject), "terrain already in a list of terrains");

        terrains.add(subject);
    }

    @Override
    public void cleanUpShader() {
        shader.cleanUp();
    }

    public TerrainShader getShader() {
        return shader;
    }
}

package engine.renders;

import engine.DisplayManager;
import entities.EntityRotations;
import entities.ambient.Camera;
import entities.ambient.Light;
import entities.ambient.Water;
import entities.ambient.WaterFrameBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import shaders.water.WaterShader;
import util.Maths;

import java.util.ArrayList;
import java.util.List;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class WaterRenderer implements Renderer<Water> {

    private final WaterShader shader = WaterShader.of();

    private final List<Water> waters = new ArrayList<>();

    private final WaterFrameBuffer frameBuffer;

    private WaterRenderer(final Matrix4f projectionMatrix, final WaterFrameBuffer frameBuffer) {
        this.frameBuffer = frameBuffer;

        shader.start();
        shader.connectTextureUnits();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
    }

    public static WaterRenderer of(final Matrix4f projectionMatrix, final WaterFrameBuffer frameBuffer) {
        checkNotNull(projectionMatrix);
        checkNotNull(frameBuffer);

        return new WaterRenderer(projectionMatrix, frameBuffer);
    }

    @Override
    public void render(final List<Light> lights, final Camera camera, final Vector4f clipPlane) {
        checkNotNull(lights);
        checkNotNull(camera);

        waters.forEach(water -> prepare(water, camera, lights.get(0)));

        waters.forEach(this::renderWater);

        unbind();
    }

    private void prepare(final Water water, final Camera camera, final Light light) {
        shader.start();
        shader.loadViewMatrix(camera);

        adjustMoveFactor();
        shader.loadLight(light);

        GL30.glBindVertexArray(water.getRawModel().getVaoID());
        GL20.glEnableVertexAttribArray(0);

        bindTextures(water);

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }

    private void bindTextures(final Water water) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, frameBuffer.getReflectionTexture());

        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, frameBuffer.getRefractionTexture());

        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, water.getDUDVTextureID());

        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, water.getNormalMapTextureID());

        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, frameBuffer.getRefractionDepthTexture());
    }

    private float moveFactor;

    private void adjustMoveFactor() {
        moveFactor += Water.WAVE_SPEED * DisplayManager.getFrameTimeSeconds();
        moveFactor %= 1;

        shader.loadMoveFactor(moveFactor);
    }

    private void renderWater(final Water water) {
        final Vector3f translation = new Vector3f(water.getX(), water.getHeight(), water.getZ());

        final EntityRotations rotations = EntityRotations.Builder.of().build();

        shader.loadModelMatrix(Maths.createTransformationMatrix(translation, rotations, Water.SIZE));

        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, water.getRawModel().getVertexCount());
    }

    private void unbind() {
        GL11.glDisable(GL11.GL_BLEND);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
        shader.stop();
    }

    @Override
    public void process(final Water subject) {
        checkNotNull(subject);
        checkArgument(!waters.contains(subject), "Water is already in a list of waters");

        waters.add(subject);
    }

    @Override
    public void cleanUpShader() {
        shader.stop();
    }
}

package entities;

import models.TexturedModel;
import org.lwjgl.util.vector.Vector3f;

import static util.Preconditions.checkNotNull;

public class Entity {

    private final TexturedModel texturedModel;

    private final Vector3f position;

    private final EntityRotations rotations;

    private final int textureIndex;

    private final float scale;

    public static class Builder implements EntityBuilder<Builder> {

        private final TexturedModel texturedModel;

        private final Vector3f position;

        private EntityRotations rotations;

        private float scale;

        private int textureIndex;

        private Builder(final TexturedModel texturedModel, final Vector3f position) {
            this.texturedModel = texturedModel;
            this.position = position;
            this.rotations = EntityRotations.Builder.of().build();
            this.scale = 1;
            this.textureIndex = 0;
        }

        public static Builder of(final TexturedModel texturedModel, final Vector3f position) {
            checkNotNull(texturedModel);
            checkNotNull(position);

            return new Builder(texturedModel, position);
        }

        @Override
        public Builder rotations(final EntityRotations rotations) {
            this.rotations = rotations;
            return this;
        }

        @Override
        public Builder scale(final float scale) {
            this.scale = scale;
            return this;
        }

        @Override
        public Builder textureIndex(int textureIndex) {
            this.textureIndex = textureIndex;
            return this;
        }

        public Entity build() {
            return new Entity(this);
        }

    }

    protected Entity(final Builder builder) {
        this.texturedModel = builder.texturedModel;
        this.position      = builder.position;
        this.rotations     = builder.rotations;
        this.scale         = builder.scale;
        this.textureIndex  = builder.textureIndex;
    }

    public void increasePosition(final float directionX, final float directionY, final float directionZ) {
        this.position.x += directionX;
        this.position.y += directionY;
        this.position.z += directionZ;
    }

    public void increaseRotation(final float directionX, final float directionY, final float directionZ) {
        this.rotations.setX(rotations.getX() + directionX);
        this.rotations.setY(rotations.getY() + directionY);
        this.rotations.setZ(rotations.getZ() + directionZ);
    }

    public float getTextureXOffset() {
        final int column = textureIndex % texturedModel.getTexture().getNumberOfRows();

        return (float) column / (float) texturedModel.getTexture().getNumberOfRows();
    }

    public float getTextureYOffset() {
        final int row = textureIndex / texturedModel.getTexture().getNumberOfRows();

        return (float) row / (float) texturedModel.getTexture().getNumberOfRows();
    }

    public TexturedModel getTexturedModel() {
        return texturedModel;
    }

    public Vector3f getPosition() {
        return position;
    }

    public EntityRotations getRotations() {
        return rotations;
    }

    public float getScale() {
        return scale;
    }

    @Override
    public String toString() {
        return "Entity:[texturedModel=" + texturedModel +
                ", position=" + position +
                ", rotations=" + rotations +
                ", scale=" + scale;
    }
}

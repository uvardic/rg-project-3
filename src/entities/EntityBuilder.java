package entities;

public interface EntityBuilder<T> {

    T rotations(final EntityRotations rotations);

    T scale(final float scale);

    T textureIndex(final int textureIndex);

}

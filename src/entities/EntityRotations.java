package entities;

public class EntityRotations {

    private float x;

    private float y;

    private float z;

    public static class Builder {

        private float x;

        private float y;

        private float z;

        private Builder() {}

        public static Builder of() {
            return new Builder();
        }

        public Builder x(final float x) {
            this.x = x;
            return this;
        }

        public Builder y(final float y) {
            this.y = y;
            return this;
        }

        public Builder z(final float z) {
            this.z = z;
            return this;
        }

        public EntityRotations build() {
            return new EntityRotations(this);
        }

    }

    private EntityRotations(final Builder builder) {
        this.x = builder.x;
        this.y = builder.y;
        this.z = builder.z;
    }

    public float getX() {
        return x;
    }

    void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    void setZ(float z) {
        this.z = z;
    }
}

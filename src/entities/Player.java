package entities;

import engine.DisplayManager;
import entities.ambient.Terrain;
import models.TexturedModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import static util.Preconditions.checkNotNull;

public class Player extends Entity {

    private static final float RUN_SPEED = 40;

    private static final float TURN_SPEED = 160;

    private static final float GRAVITY = -50;

    private static final float JUMP_POWER = 30;

    private float currentSpeed;

    private float currentTurnSpeed;

    private float upwardsSpeed;

    private boolean inAir;

    public static class PlayerBuilder implements EntityBuilder<PlayerBuilder> {

        private final Entity.Builder baseBuilder;

        private PlayerBuilder(final TexturedModel texturedModel, final Vector3f position) {
            this.baseBuilder = Entity.Builder.of(texturedModel, position);
        }

        public static PlayerBuilder of(final TexturedModel texturedModel, final Vector3f position) {
            checkNotNull(texturedModel);
            checkNotNull(position);

            return new PlayerBuilder(texturedModel, position);
        }

        @Override
        public PlayerBuilder rotations(final EntityRotations rotations) {
            baseBuilder.rotations(rotations);
            return this;
        }

        @Override
        public PlayerBuilder scale(final float scale) {
            baseBuilder.scale(scale);
            return this;
        }

        @Override
        public PlayerBuilder textureIndex(final int textureIndex) {
            baseBuilder.textureIndex(textureIndex);
            return this;
        }

        public Player build() {
            return new Player(this);
        }
    }

    private Player(final PlayerBuilder builder) {
        super(builder.baseBuilder);
    }

    public void move(final Terrain terrain) {
        checkInputs();
        increaseRotation(0, currentTurnSpeed * DisplayManager.getFrameTimeSeconds(), 0);

        final float distance = currentSpeed * DisplayManager.getFrameTimeSeconds();

        final float directionX = (float) (distance * Math.sin(Math.toRadians(getRotations().getY())));

        final float directionZ = (float) (distance * Math.cos(Math.toRadians(getRotations().getY())));

        increasePosition(directionX, 0, directionZ);
        upwardsSpeed += GRAVITY * DisplayManager.getFrameTimeSeconds();
        increasePosition(0, upwardsSpeed * DisplayManager.getFrameTimeSeconds(), 0);

        final float terrainHeight = terrain.getHeightOfTerrain(getPosition().getX(), getPosition().getZ());

        if (getPosition().getY() < terrainHeight) {
            upwardsSpeed = 0;
            getPosition().setY(terrainHeight);
            inAir = false;
        }
    }

    private void checkInputs() {
        if (Keyboard.isKeyDown(Keyboard.KEY_W))
            this.currentSpeed = RUN_SPEED;

        else if (Keyboard.isKeyDown(Keyboard.KEY_S))
            this.currentSpeed = -RUN_SPEED;

        else this.currentSpeed = 0;

        if (Keyboard.isKeyDown(Keyboard.KEY_D))
            this.currentTurnSpeed = -TURN_SPEED;

        else if (Keyboard.isKeyDown(Keyboard.KEY_A))
            this.currentTurnSpeed = TURN_SPEED;

        else this.currentTurnSpeed = 0;

        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE))
            jump();
    }

    private void jump() {
        if (!inAir) {
            upwardsSpeed = JUMP_POWER;
            inAir = true;
        }
    }
}

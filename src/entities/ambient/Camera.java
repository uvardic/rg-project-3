package entities.ambient;

import entities.Player;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

import static util.Preconditions.checkNotNull;

public class Camera {

    private static final float ZOOM_SENSITIVITY = 0.1f;

    private static final float PITCH_SENSITIVITY = 0.1f;

    private static final float ANGLE_SENSITIVITY = 0.3f;

    private static final float CAMERA_OFFSET = 3;

    private static final float MAX_ZOOM = 100;

    private static final float MIN_ZOOM = 5;

    private static final float MAX_PITCH = 100;

    private static final float MIN_PITCH = 5;

    private final Player player;

    private final Vector3f position;

    private float distanceFromPlayer;

    private float angleAroundPlayer;

    private float pitch;

    private float yaw;

    private float roll;

    public static class Builder {

        private final Player player;

        private final Vector3f position;

        private float distanceFromPlayer;

        private float angleAroundPlayer;

        private float pitch;

        private float yaw;

        private float roll;

        private Builder(final Player player, final Vector3f position) {
            this.player = player;
            this.position = position;
            this.distanceFromPlayer = 50;
            this.pitch = 20;
        }

        public static Builder of(final Player player, final Vector3f position) {
            checkNotNull(player);
            checkNotNull(position);

            return new Builder(player, position);
        }

        public Builder distanceFromPlayer(final float distanceFromPlayer) {
            this.distanceFromPlayer = distanceFromPlayer;
            return this;
        }

        public Builder angleAroundPlayer(final float angleAroundPlayer) {
            this.angleAroundPlayer = angleAroundPlayer;
            return this;
        }

        public Builder pitch(final float pitch) {
            this.pitch = pitch;
            return this;
        }

        public Builder yaw(final float yaw) {
            this.yaw = yaw;
            return this;
        }

        public Builder roll(final float roll) {
            this.roll = roll;
            return this;
        }

        public Camera build() {
            return new Camera(this);
        }

    }

    private Camera(final Builder builder) {
        this.player             = builder.player;
        this.position           = builder.position;
        this.distanceFromPlayer = builder.distanceFromPlayer;
        this.angleAroundPlayer  = builder.angleAroundPlayer;
        this.pitch              = builder.pitch;
        this.yaw                = builder.yaw;
        this.roll               = builder.roll;
    }

    public void move() {
        calculateZoom();
        calculatePitch();
        calculateAngleAroundPlayer();
        calculateCameraPosition();
        calculateYaw();
        offsetCamera();
    }

    private void calculateZoom() {
        final float zoomLevel = Mouse.getDWheel() * ZOOM_SENSITIVITY;

        distanceFromPlayer -= zoomLevel;

        limitZoom();
    }

    private void limitZoom() {
        if (distanceFromPlayer > MAX_ZOOM)
            distanceFromPlayer = MAX_ZOOM;

        if (distanceFromPlayer < MIN_ZOOM)
            distanceFromPlayer = MIN_ZOOM;
    }

    private void calculatePitch() {
        if (isRightMouseButtonPressed()) {
            final float pitchChange = Mouse.getDY() * PITCH_SENSITIVITY;

            pitch -= pitchChange;

            limitPitch();
        }
    }

    private void limitPitch() {
        if (pitch > MAX_PITCH)
            pitch = MAX_PITCH;

        if (pitch < MIN_PITCH)
            pitch = MIN_PITCH;
    }

    private boolean isRightMouseButtonPressed() {
        return Mouse.isButtonDown(1);
    }

    private void calculateAngleAroundPlayer() {
        if (isLeftMouseButtonPressed()) {
            final float angleChange = Mouse.getDX() * ANGLE_SENSITIVITY;

            angleAroundPlayer -= angleChange;
        }
    }

    private boolean isLeftMouseButtonPressed() {
        return Mouse.isButtonDown(0);
    }

    private void calculateCameraPosition() {
        final float theta = player.getRotations().getY() + angleAroundPlayer;

        final float offsetX = (float) (calculateHorizontalDistance() * Math.sin(Math.toRadians(theta)));

        final float offsetZ = (float) (calculateHorizontalDistance() * Math.cos(Math.toRadians(theta)));

        position.setX(player.getPosition().getX() - offsetX);
        position.setY(player.getPosition().getY() + calculateVerticalDistance());
        position.setZ(player.getPosition().getZ() - offsetZ);
    }

    private float calculateHorizontalDistance() {
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    private float calculateVerticalDistance() {
        return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }

    private void calculateYaw() {
        yaw = 180 - (player.getRotations().getY() + angleAroundPlayer);
    }

    private void offsetCamera() {
        position.setY(player.getPosition().getY() + calculateVerticalDistance() + CAMERA_OFFSET);
    }

    public void invertPitch() {
        this.pitch = -pitch;
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    @Override
    public String toString() {
        return "Camera:[player=" + player +
                ", position=" + position +
                ", distanceFromPlayer=" + distanceFromPlayer +
                ", angleAroundPlayer=" + angleAroundPlayer +
                ", pitch=" + pitch +
                ", yaw=" + yaw +
                ", roll=" + roll + "]";
    }
}

package entities.ambient;

import java.util.Random;

public class HeightsGenerator {

    private static final float AMPLITUDE = 70f;

    private static final float ROUGHNESS = 0.3f;

    private static final int RANDOM_BOUND = 1000000000;

    private static final int OCTAVES = 3;

    private final Random random = new Random();

    private final int seed;

    private HeightsGenerator() {
        this.seed = random.nextInt(RANDOM_BOUND);
    }

    public static HeightsGenerator of() {
        return new HeightsGenerator();
    }

    public float generateHeight(final int x, final int z) {
        final float d = (float) Math.pow(2, OCTAVES - 1);

        float total = 0;

        for (int i = 0; i < OCTAVES; i++) {
            final float freq = (float) (Math.pow(2, i) / d);
            final float amp = (float) Math.pow(ROUGHNESS, i) * AMPLITUDE;

            total += getInterpolatedNoise(x * freq, z * freq) * amp;
        }

        return total;
    }

    private float getInterpolatedNoise(final float x, final float z) {
        final int intX = (int) x;

        final int intZ = (int) z;

        final float fracX = x - intX;

        final float fracZ = z - intZ;

        final float v1 = getSmoothNoise(intX, intZ);

        final float v2 = getSmoothNoise(intX + 1, intZ);

        final float v3 = getSmoothNoise(intX, intZ + 1);

        final float v4 = getSmoothNoise(intX + 1, intZ + 1);

        final float i1 = interpolate(v1, v2, fracX);

        final float i2 = interpolate(v3, v4, fracX);

        return interpolate(i1, i2, fracZ);
    }

    private float getSmoothNoise(final int x, final int z) {
        return getCorners(x, z) + getSides(x, z) + getCenter(x, z);
    }

    private float getCorners(final int x, final int z) {
        return (getNoise(x - 1, z - 1) +
                getNoise(x + 1, z - 1) +
                getNoise(x - 1, z + 1) +
                getNoise(x + 1, z + 1)) / 16f;
    }

    private float getSides(final int x, final int z) {
        return (getNoise(x - 1, z) +
                getNoise(x + 1, z) +
                getNoise(x, z - 1) +
                getNoise(x, z + 1)) / 8f;
    }

    private float getCenter(final int x, final int z) {
        return getNoise(x, z) / 4f;
    }

    private float getNoise(final int x, final int z) {
        final int offsetX = 49632;

        final int offsetY = 325186;

        random.setSeed(x * offsetX + z * offsetY + seed);

        return random.nextFloat() * 2f - 1f;
    }

    private float interpolate(final float a, final float b, final float blend) {
        final double theta = blend * Math.PI;

        final float factor = (float) (1f - Math.cos(theta)) * 0.5f;

        return a * (1f - factor) + b * factor;
    }
}

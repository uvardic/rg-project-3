package entities.ambient;

import org.lwjgl.util.vector.Vector3f;

import static util.Preconditions.checkNotNull;

public class Light {

    private final Vector3f position;

    private final Vector3f colour;

    private final Vector3f attenuation;

    private Light(final Vector3f position, final Vector3f colour, final Vector3f attenuation) {
        this.position = position;
        this.colour = colour;
        this.attenuation = attenuation;
    }

    public static Light of(final Vector3f position, final Vector3f colour) {
        checkNotNull(position);
        checkNotNull(colour);

        return new Light(position, colour, new Vector3f(1, 0, 0));
    }

    public static Light of(final Vector3f position, final Vector3f colour, final Vector3f attenuation) {
        checkNotNull(position);
        checkNotNull(colour);
        checkNotNull(attenuation);

        return new Light(position, colour, attenuation);
    }

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getColour() {
        return colour;
    }

    public Vector3f getAttenuation() {
        return attenuation;
    }

    @Override
    public String toString() {
        return "Light:[position=" + position + ", colour=" + colour + ", attenuation=" + attenuation + "]";
    }
}

package entities.ambient;

import engine.loaders.Loader;
import models.RawModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static util.Preconditions.checkNotNull;

public class Sky {

    private static final float SIZE = 500f;

    private static final float[] VERTICES = {
            -SIZE,  SIZE, -SIZE,
            -SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,

            -SIZE, -SIZE,  SIZE,
            -SIZE, -SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE,  SIZE,
            -SIZE, -SIZE,  SIZE,

            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,

            -SIZE, -SIZE,  SIZE,
            -SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE, -SIZE,  SIZE,
            -SIZE, -SIZE,  SIZE,

            -SIZE,  SIZE, -SIZE,
            SIZE,  SIZE, -SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            -SIZE,  SIZE,  SIZE,
            -SIZE,  SIZE, -SIZE,

            -SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE,  SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE,  SIZE,
            SIZE, -SIZE,  SIZE
    };

    private final RawModel rawModel;

    private final int dayTextureID;

    private final int nightTextureID;

    private final List<File> dayTextureFiles = new ArrayList<>();

    private final List<File> nightTextureFiles = new ArrayList<>();

    private Sky(final Loader loader) {
        addDayTextureFiles();
        addNightTextureFiles();

        this.rawModel       = loader.loadToVAO(VERTICES, 3);
        this.dayTextureID   = loader.loadCubeMap(dayTextureFiles);
        this.nightTextureID = loader.loadCubeMap(nightTextureFiles);
    }

    private void addDayTextureFiles() {
        dayTextureFiles.add(new File("resources/textures/skybox/day/right.png"));
        dayTextureFiles.add(new File("resources/textures/skybox/day/left.png"));
        dayTextureFiles.add(new File("resources/textures/skybox/day/top.png"));
        dayTextureFiles.add(new File("resources/textures/skybox/day/bottom.png"));
        dayTextureFiles.add(new File("resources/textures/skybox/day/back.png"));
        dayTextureFiles.add(new File("resources/textures/skybox/day/front.png"));
    }

    private void addNightTextureFiles() {
        nightTextureFiles.add(new File("resources/textures/skybox/night/right.png"));
        nightTextureFiles.add(new File("resources/textures/skybox/night/left.png"));
        nightTextureFiles.add(new File("resources/textures/skybox/night/top.png"));
        nightTextureFiles.add(new File("resources/textures/skybox/night/bottom.png"));
        nightTextureFiles.add(new File("resources/textures/skybox/night/back.png"));
        nightTextureFiles.add(new File("resources/textures/skybox/night/front.png"));
    }

    public static Sky of(final Loader loader) {
        checkNotNull(loader);

        return new Sky(loader);
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public int getDayTextureID() {
        return dayTextureID;
    }

    public int getNightTextureID() {
        return nightTextureID;
    }

    @Override
    public String toString() {
        return "Sky:[rawModel=" + rawModel +
                ", dayTextureID=" + dayTextureID + "]";
    }
}

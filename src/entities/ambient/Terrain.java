package entities.ambient;

import engine.loaders.Loader;
import models.RawModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import models.textures.TerrainTexture;
import models.textures.TerrainTexturePack;
import util.Maths;

import static util.Preconditions.checkNotNull;

public class Terrain {

    private static final float SIZE = 800;

    private static final float MAX_HEIGHT = 40;

    private static final float MIN_HEIGHT = -40;

    private static final float MAX_PIXEL_COLOUR = (float) Math.pow(256, 3);

    private final HeightsGenerator generator = HeightsGenerator.of();

    private final float gridX;

    private final float gridZ;

    private final RawModel rawModel;

    private final TerrainTexturePack texturePack;

    private final TerrainMaps terrainMaps;

    private final int vertexCount;

    private final float[][] heights;

    public static class Builder {

        private final Loader loader;

        private final TerrainTexturePack texturePack;

        private final TerrainMaps terrainMaps;

        private float gridX;

        private float gridZ;

        private Builder(final Loader loader, final TerrainTexturePack texturePack, final TerrainMaps terrainMaps) {
            this.loader      = loader;
            this.texturePack = texturePack;
            this.terrainMaps = terrainMaps;
        }

        public static Builder of(final Loader loader, final TerrainTexturePack texturePack, final TerrainMaps terrainMaps) {
            checkNotNull(loader);
            checkNotNull(texturePack);
            checkNotNull(terrainMaps);

            return new Builder(loader, texturePack, terrainMaps);
        }

        public Builder gridX(final float gridX) {
            this.gridX = gridX;
            return this;
        }

        public Builder gridZ(final float grindZ) {
            this.gridZ = grindZ;
            return this;
        }

        public Terrain build() {
            return new Terrain(this);
        }

    }

    private Terrain(final Builder builder) {
        this.gridX       = builder.gridX * SIZE;
        this.gridZ       = builder.gridZ * SIZE;
        this.texturePack = builder.texturePack;
        this.terrainMaps = builder.terrainMaps;
        this.vertexCount = 128;
        this.heights     = new float[vertexCount][vertexCount];
        this.rawModel    = generateTerrain(builder.loader);
    }

    private float[] vertices;

    private float[] normals;

    private float[] textureCoords;

    private int[] indices;

    private RawModel generateTerrain(final Loader loader) {
        checkNotNull(loader);

        initializeArrays();
        generateTerrainData();

        return loader.loadToVAO(vertices, textureCoords, normals, indices);
    }

    private void generateTerrainData() {
        int arrayPointer = 0;

        int indicesPointer = 0;

        for(int row = 0; row < vertexCount; row++) {
            for(int column = 0; column < vertexCount; column++) {
                generateVertices(arrayPointer, row, column);
                generateNormals(arrayPointer, row, column);
                generateTextureCoords(arrayPointer, row, column);

                if (!isLastIteration(row, column)) {
                    generateIndices(indicesPointer, row, column);
                    indicesPointer += 6;
                }

                arrayPointer++;
            }
        }
    }

    private void initializeArrays() {
        final int count = vertexCount * vertexCount;

        vertices      = new float[count * 3];
        normals       = new float[count * 3];
        textureCoords = new float[count * 2];
        indices       = new int[6 * (vertexCount - 1) * (vertexCount - 1)];
    }

    private void generateVertices(final int vertexPointer, final int row, final int column) {
        final float height = getHeight(row, column);

        heights[column][row] = height;

        vertices[vertexPointer * 3]     = (float) column / ((float) vertexCount - 1) * SIZE;
        vertices[vertexPointer * 3 + 1] = height;
        vertices[vertexPointer * 3 + 2] = (float) row/ ((float) vertexCount - 1) * SIZE;
    }

    private void generateNormals(final int normalPointer, final int row, final int column) {
        final Vector3f normal = calculateNormal(row, column);

        normals[normalPointer * 3]     = normal.getX();
        normals[normalPointer * 3 + 1] = normal.getY();
        normals[normalPointer * 3 + 2] = normal.getZ();
    }

    private Vector3f calculateNormal(final int x, final int y) {
        final float heightLeft = getHeight(x - 1, y);

        final float heightRight = getHeight(x + 1, y);

        final float heightDown = getHeight(x, y - 1);

        final float heightUp = getHeight(x, y + 1);

        final Vector3f normal = new Vector3f(heightLeft - heightRight, 2f, heightDown - heightUp);

        normal.normalise();

        return normal;
    }

    private void generateTextureCoords(final int texturePointer, final int row, final int column) {
        textureCoords[texturePointer * 2]     = (float) column / ((float) vertexCount - 1);
        textureCoords[texturePointer * 2 + 1] = (float) row / ((float) vertexCount - 1);
    }

    private boolean isLastIteration(final int row, final int column) {
        return row == vertexCount - 1 || column == vertexCount - 1;
    }

    private void generateIndices(int indicesPointer, final int row, final int column) {
        final int topLeft = (row * vertexCount) + column;

        final int topRight = topLeft + 1;

        final int bottomLeft = ((row + 1) * vertexCount) + column;

        final int bottomRight = bottomLeft + 1;

        indices[indicesPointer++] = topLeft;
        indices[indicesPointer++] = bottomLeft;
        indices[indicesPointer++] = topRight;
        indices[indicesPointer++] = topRight;
        indices[indicesPointer++] = bottomLeft;
        indices[indicesPointer]   = bottomRight;
    }

    private float getHeight(final int x, final int y) {
        return generator.generateHeight(x, y);
    }

    public float getHeightOfTerrain(final float worldX, final float worldZ) {
        final float terrainX = worldX - gridX;

        final float terrainZ = worldZ - gridZ;

        final float gridSquareSize = SIZE / (float) (heights.length - 1);

        final int gridSquareX = (int) Math.floor(terrainX / gridSquareSize);

        final int gridSquareZ = (int) Math.floor(terrainZ / gridSquareSize);

        if (isGridSquareOutOfBounds(gridSquareX, gridSquareZ))
            return 0;

        final float xCoords = (terrainX % gridSquareSize) / gridSquareSize;

        final float zCoords = (terrainZ % gridSquareSize) / gridSquareSize;

        return getResult(xCoords, zCoords, gridSquareX, gridSquareZ);
    }

    private boolean isGridSquareOutOfBounds(final int gridSquareX, final int gridSquareZ) {
        return gridSquareX < 0 || gridSquareX >= heights.length - 1 ||
               gridSquareZ < 0 || gridSquareZ >= heights.length - 1;
    }

    private float getResult(final float xCoords, final float zCoords, final int gridSquareX, final int gridSquareZ) {
        if (xCoords <= (1 - zCoords))
            return Maths.barryCentric(new Vector3f(0, heights[gridSquareX][gridSquareZ], 0),
                                      new Vector3f(1, heights[gridSquareX + 1][gridSquareZ], 0),
                                      new Vector3f(0, heights[gridSquareX][gridSquareZ + 1], 1),
                                      new Vector2f(xCoords, zCoords));

        else
            return Maths.barryCentric(new Vector3f(1, heights[gridSquareX + 1][gridSquareZ], 0),
                                       new Vector3f(1, heights[gridSquareX + 1][gridSquareZ + 1], 1),
                                       new Vector3f(0, heights[gridSquareX][gridSquareZ + 1], 1),
                                       new Vector2f(xCoords, zCoords));
    }

    public float getGridX() {
        return gridX;
    }

    public float getGridZ() {
        return gridZ;
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public TerrainTexturePack getTexturePack() {
        return texturePack;
    }

    public TerrainTexture getBlendMap() {
        return terrainMaps.getBlendMap();
    }

    @Override
    public String toString() {
        return "Terrain:[gridX=" + gridX +
                ", gridZ=" + gridZ +
                ", texturePack=" + texturePack +
                ", textureMaps=" + terrainMaps +
                ", rawModel=" + rawModel + "]";
    }
}

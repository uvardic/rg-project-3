package entities.ambient;

import models.textures.TerrainTexture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static util.Preconditions.checkNotNull;

public class TerrainMaps {

    private final TerrainTexture blendMap;

    private final BufferedImage heightMap;

    private TerrainMaps(final TerrainTexture blendMap, final File heightMap) {
        this.blendMap = blendMap;
        this.heightMap = loadHeightMap(heightMap);
    }

    private BufferedImage loadHeightMap(final File heightMap) {
        try  {
            return ImageIO.read(heightMap);
        } catch (IOException ex) { ex.printStackTrace(); }

        throw new RuntimeException("Height map loading failed");
    }

    public static TerrainMaps of(final TerrainTexture blendMap, final File heightMap) {
        checkNotNull(blendMap);
        checkNotNull(heightMap);

        return new TerrainMaps(blendMap, heightMap);
    }

    public TerrainTexture getBlendMap() {
        return blendMap;
    }

    public BufferedImage getHeightMap() {
        return heightMap;
    }

    @Override
    public String toString() {
        return "TerrainMaps:[blendMap=" + blendMap + ", heightMap=" + heightMap + "]";
    }
}

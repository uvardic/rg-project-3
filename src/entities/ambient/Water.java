package entities.ambient;

import engine.loaders.Loader;
import models.RawModel;

import static util.Preconditions.checkNotNull;

public class Water {

    public static final float SIZE = 800;

    public static final float WAVE_SPEED = 0.03f;

    private static final String DUDV_MAP = "water/waterDUDV.png";

    private static final String NORMAL_MAP = "water/normalMap.png";

    private final RawModel rawModel;

    private final float x;

    private final float z;

    private final float height;

    private final int DUDVTextureID;

    private final int normalMapTextureID;

    private Water(final Loader loader, final float x, final float z, final float height) {
        final float[] vertices = {-1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1};

        this.x                  = x;
        this.z                  = z;
        this.height             = height;
        this.rawModel           = loader.loadToVAO(vertices, 2);
        this.DUDVTextureID      = loader.loadTexture(DUDV_MAP);
        this.normalMapTextureID = loader.loadTexture(NORMAL_MAP);
    }

    public static Water of(final Loader loader, final float x, final float z, final float height) {
        checkNotNull(loader);

        return new Water(loader, x, z, height);
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public float getX() {
        return x;
    }

    public float getZ() {
        return z;
    }

    public float getHeight() {
        return height;
    }

    public int getDUDVTextureID() {
        return DUDVTextureID;
    }

    public int getNormalMapTextureID() {
        return normalMapTextureID;
    }
}

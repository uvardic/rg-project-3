package models;

import static util.Preconditions.checkArgument;

public class RawModel {

    private final int vaoID;

    private final int vertexCount;

    private RawModel(final int vaoID, final int vertexCount) {
        this.vaoID = vaoID;
        this.vertexCount = vertexCount;
    }

    public static RawModel of(final int vaoID, final int vertexCount) {
        checkArgument(vaoID >= 0, "vaoID can't be a negative number");
        checkArgument(vertexCount >= 0, "vertexCount can't be a negative number");

        return new RawModel(vaoID, vertexCount);
    }

    public int getVaoID() {
        return vaoID;
    }

    public int getVertexCount() {
        return vertexCount;
    }

    @Override
    public String toString() {
        return "RawModel=[vaoID=" + vaoID + ", vertexCount=" + vertexCount + "]";
    }
}

package models;

import models.textures.ModelTexture;

import static util.Preconditions.checkNotNull;

public class TexturedModel {

    private final RawModel rawModel;

    private final ModelTexture texture;

    private TexturedModel(final RawModel rawModel, final ModelTexture texture) {
        this.rawModel = rawModel;
        this.texture = texture;
    }

    public static TexturedModel of(final RawModel rawModel, final ModelTexture texture) {
        checkNotNull(rawModel);
        checkNotNull(texture);

        return new TexturedModel(rawModel, texture);
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public ModelTexture getTexture() {
        return texture;
    }

    @Override
    public String toString() {
        return "TexturedModel:[rawModel=" + rawModel + ", texture=" + texture + "]";
    }
}

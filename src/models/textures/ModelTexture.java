package models.textures;

import static util.Preconditions.checkArgument;

public class ModelTexture {

    private final int textureID;

    private final float shineDamper;

    private final float reflectivity;

    private final boolean transparent;

    private final boolean fakeLight;

    private final int numberOfRows;

    public static class Builder {

        private final int textureID;

        private float shineDamper;

        private float reflectivity;

        private boolean transparent;

        private boolean fakeLight;

        private int numberOfRows;

        private Builder(final int textureID) {
            this.textureID = textureID;
            this.shineDamper = 1;
            this.numberOfRows = 1;
        }

        public static Builder of(final int textureID) {
            checkArgument(textureID >=0, "textureID can't be a negative number");

            return new Builder(textureID);
        }

        public Builder shineDamper(final float shineDamper) {
            checkArgument(shineDamper >=0, "shineDamper can't be a negative number");

            this.shineDamper = shineDamper;
            return this;
        }

        public Builder reflectivity(final float reflectivity) {
            checkArgument(reflectivity >=0, "reflectivity can't be a negative number");

            this.reflectivity = reflectivity;
            return this;
        }

        public Builder transparent(final boolean transparent) {
            this.transparent = transparent;
            return this;
        }

        public Builder fakeLight(final boolean fakeLight) {
            this.fakeLight = fakeLight;
            return this;
        }

        public Builder numberOfRows(final int numberOfRows) {
            this.numberOfRows = numberOfRows;
            return this;
        }

        public ModelTexture build() {
            return new ModelTexture(this);
        }

    }

    private ModelTexture(final Builder builder) {
        this.textureID    = builder.textureID;
        this.shineDamper  = builder.shineDamper;
        this.reflectivity = builder.reflectivity;
        this.transparent  = builder.transparent;
        this.fakeLight    = builder.fakeLight;
        this.numberOfRows = builder.numberOfRows;
    }

    public int getTextureID() {
        return textureID;
    }

    public float getShineDamper() {
        return shineDamper;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public boolean isTransparent() {
        return transparent;
    }

    public boolean isFakeLight() {
        return fakeLight;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    @Override
    public String toString() {
        return "ModelTexture:[textureID=" + textureID +
                ", shineDamper=" + shineDamper +
                ", reflectivity=" + reflectivity +
                ", transparent=" + transparent +
                ", fakeLight=" + fakeLight + "]";
    }
}

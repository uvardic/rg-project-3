package models.textures;

import java.util.Objects;

import static util.Preconditions.checkArgument;

public class TerrainTexture {

    private final int textureID;

    private TerrainTexture(final int textureID) {
        this.textureID = textureID;
    }

    public static TerrainTexture of(final int textureID) {
        checkArgument(textureID >= 0, "textureID can't be a negative number");

        return new TerrainTexture(textureID);
    }

    public int getTextureID() {
        return textureID;
    }

    @Override
    public String toString() {
        return "TerrainTexture=[textureID=" + textureID + "]";
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TerrainTexture) {
            final TerrainTexture other = (TerrainTexture) obj;

            return Objects.equals(this.textureID, other.textureID);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(textureID);
    }
}

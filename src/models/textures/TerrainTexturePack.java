package models.textures;

import static util.Preconditions.checkNotNull;

public class TerrainTexturePack {

    private final TerrainTexture backgroundTexture;

    private final TerrainTexture rTexture;

    private final TerrainTexture gTexture;

    private final TerrainTexture bTexture;

    private TerrainTexturePack(final TerrainTexture backgroundTexture, final TerrainTexture rTexture,
                               final TerrainTexture gTexture, final TerrainTexture bTexture) {
        this.backgroundTexture = backgroundTexture;
        this.rTexture = rTexture;
        this.gTexture = gTexture;
        this.bTexture = bTexture;
    }

    public static TerrainTexturePack of(final TerrainTexture backgroundTexture, final TerrainTexture rTexture,
                                        final TerrainTexture gTexture, final TerrainTexture bTexture) {
        checkNotNull(backgroundTexture);
        checkNotNull(rTexture);
        checkNotNull(gTexture);
        checkNotNull(bTexture);

        return new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
    }

    public TerrainTexture getBackgroundTexture() {
        return backgroundTexture;
    }

    public TerrainTexture getRTexture() {
        return rTexture;
    }

    public TerrainTexture getGTexture() {
        return gTexture;
    }

    public TerrainTexture getBTexture() {
        return bTexture;
    }

    @Override
    public String toString() {
        return "TerrainTexturePack:[backgroundTexture=" + backgroundTexture +
                ", rTexture=" + rTexture +
                ", gTexture=" + gTexture +
                ", bTexture=" + bTexture + "]";
    }
}

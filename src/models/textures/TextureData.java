package models.textures;

import java.nio.ByteBuffer;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class TextureData {

    private final int width;

    private final int height;

    private final ByteBuffer buffer;

    private TextureData(final int width, final int height, final ByteBuffer buffer) {
        this.width = width;
        this.height = height;
        this.buffer = buffer;
    }

    public static TextureData of(final int width, final int height, final ByteBuffer buffer) {
        checkArgument(width > 0, "width must be a positive number");
        checkArgument(height > 0, "height must be a positive number");
        checkNotNull(buffer);

        return new TextureData(width, height, buffer);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }

    @Override
    public String toString() {
        return "TextureData:[width=" + width +
                ", height=" + height +
                ", buffer=" + buffer +
                ']';
    }
}

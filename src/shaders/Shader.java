package shaders;

import entities.ambient.Camera;
import entities.ambient.Light;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.stream.IntStream;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public abstract class Shader {

    private static final int NUMBER_OF_LIGHTS = 4;

    private final int vertexShaderID;

    private final int fragmentShadeID;

    private final int programID;

    private static final FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

    protected Shader(final File vertexFile, final File fragmentFile) {
        checkNotNull(vertexFile);
        checkNotNull(fragmentFile);

        this.vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
        this.fragmentShadeID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
        this.programID = GL20.glCreateProgram();

        GL20.glAttachShader(programID, vertexShaderID);
        GL20.glAttachShader(programID, fragmentShadeID);

        bindAttributes();

        GL20.glLinkProgram(programID);
        GL20.glValidateProgram(programID);

        loadDefaultUniformLocations();
        getAllUniformLocations();
    }

    private static int loadShader(final File shaderFile, final int type) {
        final StringBuilder shaderSource = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(shaderFile))) {

            reader.lines().forEach(line -> shaderSource.append(line).append('\n'));

        } catch (IOException ex) { ex.printStackTrace(); }

        final int shaderID = GL20.glCreateShader(type);

        GL20.glShaderSource(shaderID, shaderSource);
        GL20.glCompileShader(shaderID);

        if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
            System.err.println("Could not compile shader!");
            System.exit(-1);
        }

        return shaderID;
    }

    public void start() {
        GL20.glUseProgram(programID);
    }

    public void stop() {
        GL20.glUseProgram(0);
    }

    public void cleanUp() {
        stop();
        GL20.glDetachShader(programID, vertexShaderID);
        GL20.glDetachShader(programID, fragmentShadeID);
        GL20.glDeleteShader(vertexShaderID);
        GL20.glDeleteShader(fragmentShadeID);
        GL20.glDeleteProgram(programID);
    }

    protected abstract void bindAttributes();

    protected abstract void getAllUniformLocations();

    private final int[] locationForLightPosition = new int[NUMBER_OF_LIGHTS];

    private final int[] locationForLightColour = new int[NUMBER_OF_LIGHTS];

    private final int[] locationForAttenuation = new int[NUMBER_OF_LIGHTS];

    private int locationForTransformationMatrix;

    private int locationForProjectionMatrix;

    private int locationForShineDamper;

    private int locationForReflectivity;

    private int locationForSkyColour;

    private void loadDefaultUniformLocations() {
        locationForTransformationMatrix = getUniformLocation("transformationMatrix");
        locationForProjectionMatrix     = getUniformLocation("projectionMatrix");
        locationForShineDamper          = getUniformLocation("shineDamper");
        locationForReflectivity         = getUniformLocation("reflectivity");
        locationForSkyColour            = getUniformLocation("skyColour");

        IntStream.range(0, NUMBER_OF_LIGHTS).forEach(this::loadLightLocations);
    }

    private void loadLightLocations(final int index) {
        locationForLightPosition[index] = getUniformLocation("lightPosition[" + index + "]");
        locationForLightColour[index]   = getUniformLocation("lightColour[" + index + "]");
        locationForAttenuation[index]   = getUniformLocation("attenuation[" + index + "]");
    }

    public void loadTransformationMatrix(final Matrix4f transformationMatrix) {
        checkNotNull(transformationMatrix);

        loadMatrix(locationForTransformationMatrix, transformationMatrix);
    }

    public void loadProjectionMatrix(final Matrix4f projectionMatrix) {
        checkNotNull(projectionMatrix);

        loadMatrix(locationForProjectionMatrix, projectionMatrix);
    }

    public void loadLights(final List<Light> lights) {
        checkNotNull(lights);
        checkArgument(lights.size() == NUMBER_OF_LIGHTS,
                   lights.size() + " lights are in the lights list, required number is: " + NUMBER_OF_LIGHTS);

        IntStream.range(0, NUMBER_OF_LIGHTS).forEach(index -> loadLightFromList(lights, index));
    }

    private void loadLightFromList(final List<Light> lights, final int index) {
        loadVector(locationForLightPosition[index], lights.get(index).getPosition());
        loadVector(locationForLightColour[index]  , lights.get(index).getColour());
        loadVector(locationForAttenuation[index]  , lights.get(index).getAttenuation());
    }

    public void loadShineVariables(final float shineDamper, final float reflectivity) {
        loadFloat(locationForShineDamper, shineDamper);
        loadFloat(locationForReflectivity, reflectivity);
    }

    public void loadSkyColour(final float read, final float green, final float blue) {
        loadVector(locationForSkyColour, new Vector3f(read, green, blue));
    }

    public abstract void loadViewMatrix(final Camera camera);

    protected void bindAttribute(final int attribute, final String variableName) {
        checkArgument(attribute >=0, "attribute can't be a negative number");
        checkNotNull(variableName);
        checkArgument(!variableName.isEmpty(), "variableName can't be an empty String");

        GL20.glBindAttribLocation(programID, attribute, variableName);
    }

    protected int getUniformLocation(final String uniformName) {
        checkNotNull(uniformName);
        checkArgument(!uniformName.isEmpty(), "uniformName can't be an empty String");

        return GL20.glGetUniformLocation(programID, uniformName);
    }

    protected void loadInt(final int location, final int value) {
        GL20.glUniform1i(location, value);
    }

    protected void loadFloat(final int location, final float value) {
        GL20.glUniform1f(location, value);
    }

    protected void loadVector(final int location, final Vector4f vector) {
        checkNotNull(vector);

        GL20.glUniform4f(location, vector.getX(), vector.getY(), vector.getZ(), vector.getW());
    }

    protected void loadVector(final int location, final Vector3f vector) {
        checkNotNull(vector);

        GL20.glUniform3f(location, vector.getX(), vector.getY(), vector.getZ());
    }

    protected void loadVector(final int location, final Vector2f vector) {
        checkNotNull(vector);

        GL20.glUniform2f(location, vector.getX(), vector.getY());
    }

    protected void loadBoolean(final int location, final boolean value) {
        float toLoad = value ? 1 : 0;

        GL20.glUniform1f(location, toLoad);
    }

    protected void loadMatrix(final int location, final Matrix4f matrix) {
        checkNotNull(matrix);

        matrix.store(matrixBuffer);
        matrixBuffer.flip();
        GL20.glUniformMatrix4(location, false, matrixBuffer);
    }

}

package shaders.entity;

import entities.ambient.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;
import shaders.Shader;
import util.Maths;

import java.io.File;

import static util.Preconditions.checkNotNull;

public class EntityShader extends Shader {

    private static final File VERTEX_FILE = new File("src/shaders/entity/glsl/entityVertexShader.glsl");

    private static final File FRAGMENT_FILE = new File("src/shaders/entity/glsl/entityFragmentShader.glsl");

    private EntityShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public static EntityShader of() {
        return new EntityShader();
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "textureCoords");
        bindAttribute(2, "normal");
    }

    private int locationForViewMatrix;

    private int locationForFakeLight;

    private int locationForNumberOfRows;

    private int locationForOffset;

    private int locationForPlane;

    @Override
    protected void getAllUniformLocations() {
        locationForViewMatrix   = getUniformLocation("viewMatrix");
        locationForFakeLight    = getUniformLocation("fakeLight");
        locationForNumberOfRows = getUniformLocation("numberOfRows");
        locationForOffset       = getUniformLocation("offset");
        locationForPlane        = getUniformLocation("plane");
    }

    @Override
    public void loadViewMatrix(final Camera camera) {
        checkNotNull(camera);

        final Matrix4f viewMatrix = Maths.createViewMatrix(camera);

        loadMatrix(locationForViewMatrix, viewMatrix);
    }

    public void loadFakeLight(final boolean fakeLight) {
        loadBoolean(locationForFakeLight, fakeLight);
    }

    public void loadNumberOfRows(final float numberOfRows) {
        loadFloat(locationForNumberOfRows, numberOfRows);
    }

    public void loadOffset(final float x, final float y) {
        loadVector(locationForOffset, new Vector2f(x, y));
    }

    public void loadClipPlane(final Vector4f vector) {
        loadVector(locationForPlane, vector);
    }
}

package shaders.skybox;

import engine.DisplayManager;
import entities.ambient.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import shaders.Shader;
import util.Maths;

import java.io.File;

import static util.Preconditions.checkNotNull;

public class SkyboxShader extends Shader {

    private static final File VERTEX_FILE = new File("src/shaders/skybox/glsl/skyboxVertexShader.glsl");

    private static final File FRAGMENT_FILE = new File("src/shaders/skybox/glsl/skyboxFragmentShader.glsl");

    private static final float ROTATION_SPEED = 1f;

    private SkyboxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public static SkyboxShader of() {
        return new SkyboxShader();
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
    }

    private int locationForViewMatrix;

    private int locationForFogColour;

    private int locationForCubMap;

    private int locationForCubMap2;

    private int locationForBlendFactor;

    @Override
    protected void getAllUniformLocations() {
        locationForViewMatrix  = getUniformLocation("viewMatrix");
        locationForFogColour   = getUniformLocation("fogColour");
        locationForCubMap      = getUniformLocation("cubeMap");
        locationForCubMap2     = getUniformLocation("cubeMap2");
        locationForBlendFactor = getUniformLocation("blendFactor");
    }

    @Override
    public void loadViewMatrix(Camera camera) {
        checkNotNull(camera);

        final Matrix4f viewMatrix = Maths.createViewMatrix(camera);

        viewMatrix.m30 = 0;
        viewMatrix.m31 = 0;
        viewMatrix.m32 = 0;

        Matrix4f.rotate((float) Math.toRadians(rotate()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);

        loadMatrix(locationForViewMatrix, viewMatrix);
    }

    private float rotation;

    private float rotate() {
        return rotation += ROTATION_SPEED * DisplayManager.getFrameTimeSeconds();
    }

    public void loadFogColour(final Vector3f fogColour) {
        loadVector(locationForFogColour, fogColour);
    }

    public void loadBlendFactor(final float blendFactor) {
        loadFloat(locationForBlendFactor, blendFactor);
    }

    public void connectTextureUnits() {
        loadInt(locationForCubMap, 0);
        loadInt(locationForCubMap2, 1);
    }
}

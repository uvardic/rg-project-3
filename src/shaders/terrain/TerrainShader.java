package shaders.terrain;

import entities.ambient.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;
import shaders.Shader;
import util.Maths;

import java.io.File;

import static util.Preconditions.checkNotNull;

public class TerrainShader extends Shader {

    private static final File VERTEX_FILE = new File("src/shaders/terrain/glsl/terrainVertexShader.glsl");

    private static final File FRAGMENT_FILE = new File("src/shaders/terrain/glsl/terrainFragmentShader.glsl");

    private TerrainShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public static TerrainShader of() {
        return new TerrainShader();
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "textureCoords");
        bindAttribute(2, "normal");
    }

    private int locationForViewMatrix;

    private int locationForBackgroundTexture;

    private int locationForRTexture;

    private int locationForGTexture;

    private int locationForBTexture;

    private int locationForBlendMap;

    private int locationForPlane;

    @Override
    protected void getAllUniformLocations() {
        locationForViewMatrix           = getUniformLocation("viewMatrix");
        locationForBackgroundTexture    = getUniformLocation("backgroundTexture");
        locationForRTexture             = getUniformLocation("rTexture");
        locationForGTexture             = getUniformLocation("bTexture");
        locationForBTexture             = getUniformLocation("gTexture");
        locationForBlendMap             = getUniformLocation("blendMap");
        locationForPlane                = getUniformLocation("plane");
    }

    public void connectTextureUnits() {
        loadInt(locationForBackgroundTexture, 0);
        loadInt(locationForRTexture, 1);
        loadInt(locationForGTexture, 2);
        loadInt(locationForBTexture, 3);
        loadInt(locationForBlendMap, 4);
    }

    @Override
    public void loadViewMatrix(final Camera camera) {
        checkNotNull(camera);

        final Matrix4f viewMatrix = Maths.createViewMatrix(camera);

        loadMatrix(locationForViewMatrix, viewMatrix);
    }

    public void loadClipPlane(final Vector4f plane) {
        checkNotNull(plane);

        loadVector(locationForPlane, plane);
    }
}

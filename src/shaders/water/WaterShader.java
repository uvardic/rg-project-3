package shaders.water;

import entities.ambient.Camera;
import entities.ambient.Light;
import org.lwjgl.util.vector.Matrix4f;
import shaders.Shader;
import util.Maths;

import java.io.File;

import static util.Preconditions.checkNotNull;

public class WaterShader extends Shader {

    private static final File VERTEX_SHADER = new File("src/shaders/water/glsl/waterVertexShader.glsl");

    private static final File FRAGMENT_SHADER = new File("src/shaders/water/glsl/waterFragmentShader.glsl");

    private WaterShader() {
        super(VERTEX_SHADER, FRAGMENT_SHADER);
    }

    public static WaterShader of() {
        return new WaterShader();
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
    }

    private int locationForViewMatrix;

    private int locationForModelMatrix;

    private int locationForReflectionTexture;

    private int locationForRefractionTexture;

    private int locationForDUDVMap;

    private int locationForMoveFactor;

    private int locationForCameraPosition;

    private int locationForNormalMap;

    private int locationForLightColour;

    private int locationForLightPosition;

    private int locationForDepthMap;

    @Override
    protected void getAllUniformLocations() {
        locationForViewMatrix        = getUniformLocation("viewMatrix");
        locationForModelMatrix       = getUniformLocation("modelMatrix");
        locationForReflectionTexture = getUniformLocation("reflectionTexture");
        locationForRefractionTexture = getUniformLocation("refractionTexture");
        locationForDUDVMap           = getUniformLocation("dudvMap");
        locationForMoveFactor        = getUniformLocation("moveFactor");
        locationForCameraPosition    = getUniformLocation("cameraPosition");
        locationForNormalMap         = getUniformLocation("normalMap");
        locationForLightColour       = getUniformLocation("lightColour");
        locationForLightPosition     = getUniformLocation("lightPosition");
        locationForDepthMap          = getUniformLocation("depthMap");
    }

    @Override
    public void loadViewMatrix(Camera camera) {
        checkNotNull(camera);

        final Matrix4f viewMatrix = Maths.createViewMatrix(camera);

        loadMatrix(locationForViewMatrix, viewMatrix);
        loadVector(locationForCameraPosition, camera.getPosition());
    }

    public void loadModelMatrix(final Matrix4f modelMatrix) {
        loadMatrix(locationForModelMatrix, modelMatrix);
    }

    public void connectTextureUnits() {
        loadInt(locationForReflectionTexture, 0);
        loadInt(locationForRefractionTexture, 1);
        loadInt(locationForDUDVMap, 2);
        loadInt(locationForNormalMap, 3);
        loadInt(locationForDepthMap, 4);
    }

    public void loadMoveFactor(final float factor) {
        loadFloat(locationForMoveFactor, factor);
    }

    public void loadLight(final Light light) {
        loadVector(locationForLightColour, light.getColour());
        loadVector(locationForLightPosition, light.getPosition());
    }

}

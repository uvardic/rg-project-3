package util;

import entities.ambient.Camera;
import entities.EntityRotations;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import static util.Preconditions.checkNotNull;

public class Maths {

    private static final float FIELD_OF_VIEW_ANGLE = 70;

    private static final float NEAR_PLANE = 0.1f;

    private static final float FAR_PLANE = 1000;

    private Maths() {}

    public static Matrix4f createTransformationMatrix(final Vector3f translation, final EntityRotations rotations, final float scale) {
        final Matrix4f transformationMatrix = new Matrix4f();

        transformationMatrix.setIdentity();

        Matrix4f.translate(translation, transformationMatrix, transformationMatrix);
        Matrix4f.rotate((float) Math.toRadians(rotations.getX()), new Vector3f(1, 0, 0), transformationMatrix, transformationMatrix);
        Matrix4f.rotate((float) Math.toRadians(rotations.getY()), new Vector3f(0, 1, 0), transformationMatrix, transformationMatrix);
        Matrix4f.rotate((float) Math.toRadians(rotations.getZ()), new Vector3f(0, 0, 1), transformationMatrix, transformationMatrix);
        Matrix4f.scale(new Vector3f(scale, scale, scale), transformationMatrix, transformationMatrix);

        return transformationMatrix;
    }

    public static Matrix4f createProjectionMatrix() {
        final float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();

        final float scaleY = (float) ((1f / Math.tan(Math.toRadians(FIELD_OF_VIEW_ANGLE / 2f))) * aspectRatio);

        final float scaleX = scaleY / aspectRatio;

        final float frustumLength = FAR_PLANE - NEAR_PLANE;

        final Matrix4f projectionMatrix = new Matrix4f();

        projectionMatrix.m00 = scaleX;
        projectionMatrix.m11 = scaleY;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustumLength);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustumLength);
        projectionMatrix.m33 = 0;

        return projectionMatrix;
    }

    public static Matrix4f createViewMatrix(final Camera camera) {
        final Matrix4f viewMatrix = new Matrix4f();

        viewMatrix.setIdentity();

        Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix, viewMatrix);
        Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);

        final Vector3f cameraPosition = camera.getPosition();

        final Vector3f negativeCameraPosition = new Vector3f(-cameraPosition.getX(), -cameraPosition.getY(), -cameraPosition.getZ());

        Matrix4f.translate(negativeCameraPosition, viewMatrix, viewMatrix);

        return viewMatrix;
    }

    public static float barryCentric(final Vector3f point1, final Vector3f point2, final Vector3f point3, final Vector2f position) {
        checkNotNull(point1);
        checkNotNull(point2);
        checkNotNull(point3);
        checkNotNull(position);

        final float determinant = (point2.getZ() - point3.getZ()) *
                                  (point1.getX() - point3.getX()) + (point3.getX() - point2.getX()) *
                                  (point1.getZ() - point3.getZ());

        final float line1 = ((point2.getZ() - point3.getZ()) *
                            (position.getX() - point3.getX()) + (point3.getX() - point2.getX()) *
                            (position.getY() - point3.getZ())) / determinant;

        final float line2 = ((point3.getZ() - point1.getZ()) *
                            (position.getX() - point3.getX()) + (point1.getX() - point3.getX()) *
                            (position.getY() - point3.getZ())) / determinant;

        final float line3 = 1.0f - line1 - line2;

        return line1 * point1.getY() + line2 * point2.getY() + line3 * point3.getY();
    }

}
